﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        private static readonly string[] FirstNames = new[]
        {
        "Максим", "Семен", "Роман", "Егор", "Станислав", "Вадим", "Игорь"};

        private static readonly string[] LastNames = new[]
        {
        "Гурьянов", "Демидов", "Васин", "Учевадов", "Семенов", "Ляхин",
         };
        static async Task Main(string[] args)
        {
            #region Получение клиента с сервера по айди 
            Console.WriteLine("Введите Id клиента:");
            var id = Console.ReadLine();

            var client = new HttpClient();
            var response = await client.GetAsync($"http://localhost:5000/customers/{id}");
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                Console.WriteLine(result);
            }
            else
            {
                Console.WriteLine(response.StatusCode);
            }

            Console.ReadKey();
            #endregion

            #region Создание клиента на сервере и получение по айди 

            Console.WriteLine("Генерация клиента...");
            var customer = RandomCustomer();
            var json = JsonConvert.SerializeObject(customer);
            var httpContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            Console.WriteLine("Создание клиента на сервере...");
            response = await client.PostAsync($"http://localhost:5000/customers", httpContent);
            if (response.IsSuccessStatusCode)
            {
                id = await response.Content.ReadAsStringAsync();
                var result = await client.GetStringAsync($"http://localhost:5000/customers/{id}");
                Console.WriteLine("Созданный клиент...");
                Console.WriteLine(result);
            }
            else
            {
                Console.WriteLine("Такой клиент уже создан " + response.StatusCode);
            }

            client.Dispose();
            Console.ReadKey();
            #endregion
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            return new CustomerCreateRequest
            {
                Firstname = FirstNames[new Random().Next(FirstNames.Length)],
                Lastname = LastNames[new Random().Next(LastNames.Length)],
            };
        }
    }
}