using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly IRepository<Customer> _customerRepository;
        public CustomerController(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [HttpGet("{id:long}")]
        public async Task<IActionResult> GetCustomerAsync([FromRoute] long id)
        {
            var customer = await _customerRepository.GetAsync(id);
            if (customer is null)
            {
                return StatusCode(404);
            }

            return Ok(customer);
        }

        [HttpPost("")]   
        public async Task<IActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {
            var customers = await _customerRepository.GetAllAsync();
            if (customers.FirstOrDefault(c => c.Firstname.Equals(customer.Firstname) && c.Lastname.Equals(c.Lastname)) is null)
            {
                return Ok(await _customerRepository.AddAsync(customer));
            }
            else
            {
                return StatusCode(409);
            }
        }
    }
}