﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Abstractions;

namespace WebApi.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T?> GetAsync(long id);
        Task<long> AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(long id);
    }
}