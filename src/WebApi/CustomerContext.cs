﻿using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi
{
    public class CustomerContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public CustomerContext()
        {
            Database.EnsureCreated();
        }
        public CustomerContext(DbContextOptions<CustomerContext> options) : base(options)
        {

        }
    }
}